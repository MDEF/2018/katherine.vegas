---
title: Design Studio 2 Term
period:
date: 2018-12-16 12:00:00
category: final
published: true
---

### Escaping Reality.

For this term presentation I decided to explore two paths. One is the addiction of the entertainment industry and how it should be displayed using the same methods used in pharmaceutical prescription drugs , the other the exploration on how to, and why, is important to know yourself in the digital age.


![]({{site.baseurl}}/inserto-01.jpg)
![]({{site.baseurl}}/inserto-02.jpg)
![]({{site.baseurl}}/inserto-03.jpg)
![]({{site.baseurl}}/inserto-04.jpg)

<iframe width="560" height="315" src="https://www.youtube.com/embed/QXVJoosPQkM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

#### Feedback

Talking to the guests We covered some important points, and were very helpful to be able to take it to the next level.
We discussed about the scenarios of usage of VR, the relation between personal and online identity, an analysis tool, diagnosis. how to crate an holistic approach. The sugar metaphor. The social dimension of this project, how to not validate what social media is doing with our data, the scope of our decisions. The meta data. The real reach of VR, how to make it maybe in a more established and reachable technology.

#### References.

[Replika AI](https://replika.ai/)

[Monolog of the algorithm](https://vimeo.com/249633335)

[The machine to be another](http://www.themachinetobeanother.org/)

[The future of story telling](https://futureofstorytelling.org/speaker/todd-yellin)

### Conclusion

I believe so much in the social importance of the topic that I am addressing in my project. I have always thought that to be able to generate a crucial change in humanity's path, what needs to change is humans. I see more and more the weak signals about it, new generations have come with a new discourse about awareness, love, society. I see the trends about self worth and self love, a growing number of techniques to self improvement, to get to know yourself.
I, as a designer for emergent futures, think that the path to follow now is to discover how to make a design intervention to push forward those "positive" weak signals, but, even that some time ago I was relentless of doing a 1st person experience, because I see the urgent need for a collective intervention, the reflection that I made in the past weeks has lead me to think that the first step in this project is my own self intervention. Apply what you preach and all. Also I have in myself the perfect subject to intervene, as I have said before, first individual and then collective is the way I believe this issue should be tackled. So my next step is to put myself in the center, explore all the versions on myself (online, offline), and see if I manage to unhack me, before trying to unhack you. Can I stop escaping reality?
