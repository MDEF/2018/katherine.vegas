---
title: Design Studio Midterm
period:
date: 2018-12-16 12:00:00
category: final
published: true
---

![]({{site.baseurl}}/midterm1.jpg)

We as humans have a tendency to look always at outside answers to our problems and questions. God, political and economic systems, medicine, money, almost everything in our society is based on external fixing. But those stories that we created have lend us in the edge of annihilation. Is not working anymore, not for the world and not for us. We have created all of these amazing technologies, that we now blame for the bad prognosis of our future. But you know what? After the research that has brought me to the stage I am today, I am more than convinced that what really needs fixing is ourselves.

![]({{site.baseurl}}/midterm2.jpg)

I started looking on why and how humans have a need to escape reality. That made me discover about how unfulfilled we are with our own lives, and that’s why we are always looking to escape, alcohol, drugs, social media, games your weapon of choice. And off course corporations in the entertainment industry are taking advantage of that to keep the money flowing.

![]({{site.baseurl}}/midterm3.jpg)


Some say that we are facing and issue that humanity never faced before, other know us better than we do. the human brain can and is being hacked by corporations to manipulate our decisions, feelings and opinions. I think we have been hacked since power became an institution, or even before. There is a reason on why religion, kings, presidents, capitalism, communism, or whatever story we believe in has thrive. There all ways to escape. Escape from ourselves, and most humans don't want to deal with all you need to acknowledge to really know yourself. There are things that a lot of us don't want to know about ourselves, now we create an online self, and we live behind that mask, thinking that nobody knows our deepest secrets. But it turns out, that those algorithms know you better than you know yourself. I can’t fix the past, all the ways we have been hacked along our history, but we can change the future, we can realize that technology is just a mirror to our society and stop blaming it, it doesn’t need fixing, we do.

![]({{site.baseurl}}/midterm4.jpg)


I am trying to understand all our drives, looking from paleoarchaeology to current neuropsychology. From historians, journalists, philosophers, thinkers… to try and built, as a designer, into action and not theory, a way to create awareness on this topic. We have thrived as a collective, not as the fierce competition some believe in, and as such, this topic also need the combination between individual and collective action.  

![]({{site.baseurl}}/midterm5.jpg)

![]({{site.baseurl}}/midterm6.jpg)
