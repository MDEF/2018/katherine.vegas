---
title: Atlas of weak signals
period: March- April 2019
date: 2018-10-21 12:00:00
category: term2
published: true
---

### Definitions:

In this course we got to study 5 main topics and develop speculations about 25 weak signals based on them.
For me this was a game changer, knowing how to look for those tiny signals that can cause a tremendous impact on the future, seeing all the connections between them, and thinking how can we change the course of its development.
All of the weak signals we study were a little "doomsday", that for me was a bit shocking, why there was not something positive, then I think, positive doesn't need fixing. But it's nice still to have explore it a bit, some good paths that we can also see coming, and how we could help them go faster, because that is what I am trying to do with my personal project.
I now have changed my way of looking at problems, analyzing and searching for solutions. For us, designers of emergent futures, I believe this is the core of our profession.

In the first exercise of our training we got to work on a topic called "surveillance capitalism" and that was directly related to my personal project.
So we had weak signals to study but I decided to work on a new one called manipulation. With my group we did this speculation on how the world look like without advertisement, since is the primary tool of capitalism and is the reason they micro target us trough our data:

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQ7KR94VX_9wUaNs-nlu5RSiJ_UKUM9JYiTONX9mWJMq_e4z2gRuarm7bN0Ym6JBPWILf4uCOg2P_O8/embed?start=false&loop=false&delayms=5000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

The second exercise was about climate consciousness in which we did a speculation on how our decisions impact the future and how we can manage to feel those impacts now to create a kind of awareness trough experience that we believe is stronger, then we also worked on the future of jobs, in which we did a speculation about how a new kind of job, the transitioner, who is a person that helps you change career paths in a constant reinvent yourself society, and how using a linkedin add that can be placed.

The last one was the World governance:

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vT7_eep4SfKwSGUgOfqhK-UwX_PezZQRj4ZhZ0umx-fnyqybSK5-VMSBncb1rlUpUukKcA1Mb4lEuvO/embed?start=false&loop=false&delayms=5000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

In this one the group somehow ended very close to my personal project, so this was an important exercise for me also. We got to see a very distopian scenario and how a resistance about the data privacy could be managed.

### Development:

In the development part of this course, we got to know how to discern, characterize, scan, search and organize this weak signals.
We learned about data scrabbing, semiotic mapping, how to manage all the data, keywords and resources to develop a map of all the weak signals given to us and also added according to our personal projects. As I said before, all of these tools are very important for me, they opened my mind to be more aware of those tiny signals in human behavior that can shape the future, how that can appear, but most of all, how can we intervene about it.  

#### Examples:

Map of the weak signals.

<iframe
  src="https://embed.kumu.io/2c7f4dfcfa5c2787b97e3fdb991f90b0"
  width="940" height="600" frameborder="0"></iframe>


My map of weak signals and research fields.

<iframe
  src="https://embed.kumu.io/1d93132ddfedc7bd1f5443cded844c82"
  width="940" height="600" frameborder="0"></iframe>
