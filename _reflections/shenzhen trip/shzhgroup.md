---
title: Group presentation
period: January 2019
date: 2018-12-16 12:00:00
category: shenzhen
published: true
---



<iframe src="https://player.vimeo.com/video/322255081" width="640" height="360" frameborder="0" allowfullscreen></iframe>


![]({{site.baseurl}}/Slide01.jpg)
![]({{site.baseurl}}/Slide02.jpg)
![]({{site.baseurl}}/Slide03.jpg)
![]({{site.baseurl}}/Slide04.jpg)


### Living:

When it comes to urbanization in Shenzhen, new buildings preserve more privacy spaces in comparison to urban villages and the digital platforms make the individualistic living more comfortable. The interesting contrast I found during my stay was that the both places have similar services, however during weekdays the non-developed urban village is constantly filled with people during the whole day and the use of its public space is so diverse while the developed one is mainly used for weekends. As I observed the non-developed urban village often has fewer private spaces and lacks some equipment, hence the residents seem to look for the functions lacked in their places in the public spaces.

![]({{site.baseurl}}/Slide05.jpg)
![]({{site.baseurl}}/Slide06.jpg)

### Communicating:

When we enter to the field of COMMUNICATION and how language is interpreted, this trip showed us and made us experience a real and tangible gap. Also it made us realized that no longer is the English language the universal one, if not the digital.
We try several times to force our spoken and even gesture language in order to be understood, but we ended up using a digital device to get to the point.
The challenge is working out these new ways of communication, relying in the reality that we still have our contrasts, our differences and even our traditions, and hoping to reduce this gap and to reconcile in a common exchange.

![]({{site.baseurl}}/Slide07.jpg)
![]({{site.baseurl}}/Slide08.jpg)


### Behaving:

We all live in this technology driven era either we realize it or not. But maybe in Shenzhen it is more obvious as it is the capital of technological innovation and we see it everywhere around us –from robot cameras to interactive mirrors, and that for sure created certain expectations before going to this trip. Yet it is interesting to see how  prejudices fall apart regarding China. Before arriving, we had this idea that people live and behave in a way so totally different than us, but the truth is there is no separating line between “us” and “them”. In fact there are more similarities than differences. For example if we observe people using public transportation, we can see more or less the same values; people absorbed in their smartphones –maybe not using the same apps but kind of a Chinese version of them.  They may be more willing to check new technologies and more familiar with the idea of change, and for sure have different approach and aesthetic, but on the other hand we see that the actual use of the technological media is not that unlike.

![]({{site.baseurl}}/Slide09.jpg)
![]({{site.baseurl}}/Slide10.jpg)

### Experiencing:

They get things done, but we have learned the hard way that society really needs to think about what and how we do it, even more now with pressing issues like AI, and climate change that needs cooperation in all levels and in all parts of the world or it really can’t be tackled. And after all, as I said before, we are all humans, we have the same basic needs and desires. That’s why we see them also glued at their phones, same patrons of addiction and needs of escape as we all do. And the most obvious, they do live in the same planet as we do, even if they forget about us or we of them. So we need them to get on board to some level of worry about this stuff, and not only keep developing, as we do need to get onboard their mentality of getting out of the thinking and into the doing. As all in this universe is dual, and the best part of it is when they converge.


#### Participants:

[Ryota Kamio](https://mdef.gitlab.io/ryota.kamio/shenzhen/)
[Julia Quiroga ](https://mdef.gitlab.io/julia.quiroga/china/)
[Vicky Simitopoulou](https://mdef.gitlab.io/vasiliki.simitopoulou/)
[Katherine S Vegas G](https://mdef.gitlab.io/katherine.vegas/reflections/researchtrip/)