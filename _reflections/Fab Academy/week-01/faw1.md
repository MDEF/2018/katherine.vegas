---
title: 1-Project Management
period: January 23
date: 2019-01-23 12:00:00
category: fabacademy
published: true
---

At the beginning of the MDEF course we were given a basic git template in which we will document most of the 1 term. In the middle of this term we had a workshop by Ilja Panic, one of our classmates, about jekyll to be able to make our own personalized web page based on html and scss, and visualize it in real time thanks to the jekyll server, it allows to see changes and if they work before doing all the pushing to the "real" one
To be able to understand the why and not only the how (as it is my personal need to be able to work on something) I did the Codeacademy course on HTML and CSS and began to create my web page based on the Jekyll template, provided by Ilja, done in HTML.
Still I encounter some troubles while learning this very complicated and specific world of web developing and code, in which a space more or less can change everything, but finally managed with some help, to upload this site in which you are reading this post, adding a special section for fab academy in my reflections space, but still is a working progress, I am not the best with graphic design.
I enjoy working with git, I find is a very easy way to have a web page repository for documentation, the process is very foolprof as you cant upload anything without making sure you have the last version with gitpull first, then to upload you need to stage all the changes you have made and make a commit, that help you see what you have done in case of a failure in the pipelines, then you push it from the local version on your computer to the remote server github or gitlab provides and its online. Being able now to understand how everything works, having done the HTML course, all is mostly logic in the code world, you can find and fix all in a very intuitive and logic way.

### Process:

#### 1:

I did a .md file with the category and permalink for fabacademy. This to make a section only for the fabacademy documentation.

![]({{site.baseurl}}/pm3.jpg)

#### 2:

Add the HTML file with the category block. Stage, commit and push.

![]({{site.baseurl}}/pm2.jpg)


#### 3:

Create folder in the reflection's one with the subfolders for each week, to keep everything organized, create a .md file to write and put the links to the images.

![]({{site.baseurl}}/pm1.jpg)

In this way I manage to have every week in their own folder with the images, videos and files and the MD file to write and display the content that will be shown on the site.

As I said before, the combination between an HTML structure with scss for styling, and markdown files to make it easy to write the content for me is the best middle way of having a personalized web page. 

### Final project.

I don't have a final project yet, you can get a glimpse of the work so far in [week 12 of term 1](https://mdef.gitlab.io/katherine.vegas/reflections/design-dialogues/ "design dialogues") and follow my project on the [Design Studio](https://mdef.gitlab.io/katherine.vegas/reflections/designstudio/) on  my reflections.
