---
title: 9-Molding and casting.
period: January - June 2019
date: 2018-12-16 12:00:00
category: fabacademy
published: true
---
### Molding:

 For this assignment I wanted to do a plant pot.

 I did the design in sketchup and then 3Dprinted the positive mold to make the negative mold. It could not be made substractively because it has angles in X,Y and Z and on two sides.

 ![]({{site.baseurl}}/curaprint.gif)
 ![]({{site.baseurl}}/printingmold.gif)
 ![]({{site.baseurl}}/printedmold1.jpg)

 Then because the mold was deep and had all the angles I needed to use a silicon that was on the less hard spectre, there is a range for the "shore A hardness" that we can check on the technical overview. After that I choose to use the "formsil 25", mix 100 part A to 5 part B.

 ![]({{site.baseurl}}/molding.gif)
 ![]({{site.baseurl}}/molding5.jpg)
 ![]({{site.baseurl}}/molding2.jpg)
 ![]({{site.baseurl}}/molding4.jpg)

 Then to take it out was still very hard, I forgot to put the releaser and the angles made it harder, after trying a lot, with the pressured air gun a screw driver we manage to take it out, I realized then the need for a softer silicone, if not it would have been impossible to take out and I would have needed to brake the mold, but in this step I could not do it because my mold had the two parts in one.

Next step was to do the second part of the nevative, the exterior one.

 ![]({{site.baseurl}}/molding1.jpg)

I waited for 24 hours as specified on the data sheet, but maybe because it was too big and too closed when I begin to take it out it still was not dry. So I waited 24 hours more.

![]({{site.baseurl}}/molding3.jpg)

 Then I still had to break the mold, one because the releaser didn't help that much, two because all the angles made very hard to take out that big of a piece, tree because the mold was printed on low quality the silicone went in the layers and got more stuck, but I finally made to take it out. It was the final part so I didn't needed the mold more.

 ![]({{site.baseurl}}/molding3.jpg)
 ![]({{site.baseurl}}/break.gif)

#### Result:

![]({{site.baseurl}}/finalmold.gif)

### Casting:

I used the liquid plastic Smooth-cast 327, because my final object is a plant pot I am not in need of a very hard or resilient material, but I did wanted the plastic finish and lightness.

![]({{site.baseurl}}/casting1.jpg)

I weighted the part A and part B, and added the colorant in the part B as recomended.

![]({{site.baseurl}}/casting3.jpg)
![]({{site.baseurl}}/casting2.jpg)

Then mixed the parts, but the color went lighter, so I added a bit more.

![]({{site.baseurl}}/casting4.jpg)

Then to get the liquid even and well distributed I poured into the outside part of the mold to then introduce the inside one, like that I am sure it gets everywhere in all the angles of my design. I prepared too much of the plastic so I had to take some out because when I introduced it went over the finish level.
So I take half out and then do it again. Clean the edges and wait for the cured time, in this case 2-4 hours were indicated in the label.

![]({{site.baseurl}}/casting.gif)

I left the excess of plastic in the cup to have a control sample of the cured process. After an hour it has mostly dried, but my casting was not, and had expanded outside the mold.

![]({{site.baseurl}}/expand.jpg)

After it dried I sanded the edges to give a smooth finish.

![]({{site.baseurl}}/sanded.gif)


#### Result:

![]({{site.baseurl}}/finall.jpg)

#### Files:

-[Plant Pot Design STL]({{site.baseurl}}/printpotmodl.stl)
