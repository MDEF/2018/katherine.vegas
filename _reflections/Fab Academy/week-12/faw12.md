---
title: 12-Aplications and implications
period: January - June 2019
date: 2018-12-16 12:00:00
category: fabacademy
published: true
---
### Project.

My project is about escaping reality and the entertainment industry.
Since as long as I could find out, humans have found a way to escape reality, from intoxication by drinking fermented liquids, psychoactive plants, gather across a fire to share stories, dances, religion; to now, television, social media, parties, games, and all the entertainment industry can come up with to keep feeding that need deep rooted in our brains.
I decided to explore the area between human behavior and technology that nowadays allows and drives us to escape.
After a lot of research, and talk with the guests in our first presentation and a lot of amazing feedback, I was inclined to do an intervention about ourselves. In that I mean, the discovery of our potential, the freewill, the sovereignty of our conditions, the anti manipulation and hacking by corporations, and how can we use all those technologies for that purpose. But more than just work with the technology I would like to work with the human, with the help of tools such as technology. Some say that we are facing and issue that humanity never faced before, other know us better than we do. the human brain can and is being hacked by corporations to manipulate our decisions, feelings and opinions.
Escape from ourselves, and most humans don't want to deal with all you need to acknowledge to really know yourself. There are things that a lot of us don't want to know about ourselves, now we create an online self, and we live behind that mask, thinking that nobody knows our deepest secrets. But it turns out, that those algorithms know you better than you know yourself. I can’t fix the past, all the ways we have been hacked along our history, but we can change the future, we can realize that technology is just a mirror to our society and stop blaming it, it doesn’t need fixing, we do.

#### Intervention:

I believe so much in the social importance of the topic that I am addressing in my project. I have always thought that to be able to generate a crucial change in humanity’s path, what needs to change is humans. I see more and more the weak signals about it, new generations have come with a new discourse about awareness, love, society. I see the trends about self worth and self love, a growing number of techniques to self improvement, to get to know yourself. I, as a designer for emergent futures, think that the path to follow now is to discover how to make a design intervention to push forward those “positive” weak signals, but, even that some time ago I was relentless of doing a 1st person experience, because I see the urgent need for a collective intervention, the reflection that I made in the past weeks has lead me to think that the first step in this project is my own self intervention. Apply what you preach and all. Also I have in myself the perfect subject to intervene, as I have said before, first individual and then collective is the way I believe this issue should be tackled. So my next step is to put myself in the center, explore all the versions on myself (online, offline), and see if I manage to unhack me, before trying to unhack you. Can I stop escaping reality?


#### What will it do?

I believe that it will generate a first step toward creating awareness and also helping me to understand how can we overcome this issue in a larger scale.

#### Who's done what beforehand?

I havent found a project similar to this one, there is a lot of talk about the subject that you can check on my research explained on my [final intervention section](https://mdef.gitlab.io/katherine.vegas/reflections/final/).


#### What will you design?

I will design a space to communicate this self journey of discovering my online self. A 21st century self portrait.

#### What materials and components will be used?

I will use digital tools like software (after effects and premiere) to edit a video  documentary of the journey, python coding to make the scrapping of the code data that was given to me by the social media. Text mining tools and image generators by machine learning to try and classify and generate the data. 


#### What questions need to be answered?

Why do we need to escape? Why the world doesn't fulfill us? What societal standards affect this reality? Where does addiction comes from? What are the neurological differences within addiction patterns? How is attention captured? Does procrastination affect this? What is the relation with unconscious desires that can be hacked? What is the relationship between hacking and free will? What weak signals and trends can we identify for the futures?

#### How will it be evaluated?

I will be fulfilled by being able to prove my hypothesis and create some awareness on the topic that I believe can have a mayor impact on our society.
I will feel good if I can manage to do a good display of my experience by making a space that can transfer my discoveries and the need to go through that process of knowing your online self.
