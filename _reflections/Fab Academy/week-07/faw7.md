---
title: 7-Computer controlled machining.
period: March 06
date: 2018-12-16 12:00:00
category: fabacademy
published: true
---

### Assignment:

For tis week we were asked to make something big on the CNC with half of a plywood board. My partner was [ Gabor Mandoki](https://mdef.gitlab.io/gabor.mandoki/)
I decided to make  a shelf.(as I have mentioned I refuse to do something that is not usable in the long term)
It was needed to add dogbones of 6mm in every inside corner of the design because the mill (6mm) cant make a straight corner in that direction.

![]({{site.baseurl}}/cnccad0.JPG)

[finalfile]({{site.baseurl}}/reflections/fabacademy/week-07/finalcncfile.dfx)


But then as we were sharing the board, my design didn't fit. So I reduce the number of shelves and made Gabor's design fit too.

![]({{site.baseurl}}/cnccad1.JPG)

Then we prepared the file on rhino cam, using a 6mm mill. The rhino cam is the process to generate the coordinates the machine will use. There you need to say if the machine will mill from outside or inside of the vectors you draw, if it will cut all the way in that is called pockets, if it will mark or just do the outlines. We did tree cam strategies, one for the screws, to after fix the board to the table of the machine so it won't move while milling the rest, then one for the outlines and one for the pockets. we checked the values for speed moving trhough the outlines and speed of rotation of the mill, for the material MDF.
When using the CNC is very important to always have glasses on because parts of the material can fly to your eyes, also always be near to check for any error that can happen and press the stop button, sound is a very important way of knowing if everything is going ok.

![]({{site.baseurl}}/cnccode.gif)

![]({{site.baseurl}}/cnc3.jpg)

![]({{site.baseurl}}/cnc1.jpeg)


The result after sanding and putting everything together:

![]({{site.baseurl}}/cnc2.jpeg)

#### Files:

-[strategy screws]({{site.baseurl}}/1-screws-6o.nc)

-[strategy Pockets and profiles]({{site.baseurl}}/2-Pocketing-Profiling-6o.nc)

-[Final DXF CNC]({{site.baseurl}}/cncgaborykt.dxf)
