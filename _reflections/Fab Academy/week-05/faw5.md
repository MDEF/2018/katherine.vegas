---
title: 5-3D Printing
period: February 20
date: 2018-12-16 12:00:00
category: fabacademy
published: true
---

### Assignment

For this week I used the scan to scan myself. Using a XBOX 360 kinetic camera and the software skanect. I was disappointed on learning that you can't scan small objects. You need to take goo care of the light and position of the scan to get the better result. My head had a hole in the top, thankfully the software filled in with a command.

![]({{site.baseurl}}/scan1.jpg)
![]({{site.baseurl}}/scan2.jpg)
![]({{site.baseurl}}/scan3.jpg)
![]({{site.baseurl}}/scan4.jpg)
![]({{site.baseurl}}/scan5.jpg)
![]({{site.baseurl}}/scan6.jpg)
![]({{site.baseurl}}/scanfinal.gif)

Example of the model of me on cura. It couldn't be made subtractively because the angles of the triangles I used for the design are to acute, also the waste of material would be to big, and the flexibleness that I needed is better with the plastic used in the 3D printing. For having the full potential of a 3D printer we need to do thing that for milling would need not only the X and Y angle but also a Z angle. pieces that have those requirements and need to be one piece.

![]({{site.baseurl}}/curame.PNG)

Then for the printing I decided to do a model for a band that will be printed in flexible filament.


![]({{site.baseurl}}/print3.jpg)
![]({{site.baseurl}}/print2.jpg)
![]({{site.baseurl}}/print1.jpg)
![]({{site.baseurl}}/printfin.gif)
![]({{site.baseurl}}/printfin2.gif)


Because the printer was smaller than my design (30cm) I had to do it in two parts, I thought it wasn't going to be a problem but at the end the part that fits in the hole to close it had not the proper form. Maybe it was too small or something so it melted and got shorter and bigger, then asking one of the tutors it turned out that I should have checked the parameter of cooling so the machine goes away for some seconds and allows the material to cool and dry before applying the next layer when we have small parts. Lesson learned.

![]({{site.baseurl}}/finalprint.jpeg)

#### Files:

-[Band.stl]({{site.baseurl}}/banda.stl)
-[scan.stl]({{site.baseurl}}/mescan.stl)
