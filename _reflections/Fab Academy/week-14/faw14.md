---
title: 13-Machine design.
period: January - June 2019
date: 2018-12-16 12:00:00
category: fabacademy
published: true
---

For this week I would like to reference our project in the 1st term of the master, in one of the introduction to digital fabrication classes called "from bits to atoms." We did a project called ["Sassy plant"](https://mdef.gitlab.io/katherine.vegas/reflections/from-bits-to-atoms/).
