---
title: 17-Wild card week
period: January - June 2019
date: 2018-12-16 12:00:00
category: fabacademy
published: true
---

### Welding:

In this class we learned how to weld two metal pieces together using the MIG method. The machine comes with the filament that is a mix between steal and flux and the gas, then it has a voltage in the gun and a negative that needs to be connected to the piece to close the circuit. It's necessary to always use protection, a proper mask, fireproof clothes and leather gloves. then you set up the voltage and the speed in which the filament will come out.

![]({{site.baseurl}}/start.jpg)
![]({{site.baseurl}}/weld.gif)

It's important to do it slowly and in circles, with a continuous movement, the bigger the better, but careful because if you have a very thin material it will create a hole, at the end you will sand it to create a smooth surface, also taking good protection to your eyes and making sure the sparks will come in your direction and not outside.

![]({{site.baseurl}}/sand.gif)

![]({{site.baseurl}}/finish.jpg)

It's important also to know the material you are aiming to work with, the machines and the filament are made for some kinds of metal or another, so be careful of what you use it for. I wanted to repair two neckless that are broken, to not always make new things but also repair something, I thought I could use this method, but the material in which they are made is a kind of aluminum that the machine we have in the lab will not work for, this machine work for steel. 
