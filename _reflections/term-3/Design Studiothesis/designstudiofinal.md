---
title: Manifesto.
period:
date: 2018-12-16 12:00:00
category: final
published: true
---
![]({{site.baseurl}}/coverbigfinal.jpg)


### Area of interest:

Since as long as I could find out, humans have found a way to escape reality, from intoxication by drinking fermented liquids, psychoactive plants, gathering across a fire to share stories, dances and religion. To now with television, social media, parties, games, and all the entertainment industry can come up with to keep feeding that need deep rooted in our brains.
I decided to explore the area between human behavior and technology that nowadays allows and drives us to escape.

*Why do we need to escape? Why doesn't the world fulfill us? What societal standards affect this reality? Where does addiction comes from? What are the neurological differences within addiction patterns? How is attention captured? Does procrastination affect this? What is the relation with unconscious desires that can be hacked? What is the relationship between hacking and free will? What weak signals and trends can we identify for the futures?*

After a lot of research, and talks with the guests in our first presentation and a lot of amazing feedback, I was inclined to do an intervention about ourselves. By that I mean, the discovery of our potential, freewill, the sovereignty of our conditions, the resistance against manipulation and hacking by corporations, and how we can use all those technologies for that purpose. But more than just work with the technology I would like to work with the human, with the help of tools such as technology, and I feel that now, every aspect of my life, professional and personal is coming together in this project and I feel it is more special and has great potential.

### Area of intervention:

I am of the opinion that we as humans have a tendency to always look at outside answers to our problems and questions. God, political and economic systems, medicine, money, almost everything in our society is based on external fixing. But those stories that we created have led us to the edge of annihilation. It is not working anymore, not for the world and not for us. We have created all of these amazing technologies that we now blame for the bad prognosis of our future. But you know what? After the research that has brought me to the stage  I am today, I am more than convinced that what really needs fixing is ourselves.
I started looking on why and how humans have a need to escape reality. That made me discover about how unfulfilled we are with our own lives, and that’s why we are always looking to escape: alcohol, drugs, social media, games — your weapon of choice. And of course corporations in the entertainment industry are taking advantage of that to keep the money flowing.

Some say *(1)*  that we are facing an issue that humanity has never faced before, others know us better than we do. The human brain can and is being hacked by corporations to manipulate our decisions, feelings and opinions *(2)*. I think we have been hacked since power became an institution, or even before. There is a reason why religion, kings, presidents, capitalism, communism, or whatever story we believe in has thrived. There are always ways to escape. Escape from ourselves, and most humans don't want to deal with all you need to acknowledge to really know yourself.  There are also theories about how ignorance is strategically manufactured, “agnotology” is the term used to describe this process *(3)*, yes I believe that there is a manufactured ignorance but I do believe that we are comfortable in it, there are things that a lot of us don't want to know about ourselves. Now we create an online self, and we live behind that mask, thinking that nobody knows our deepest secrets. But it turns out, those algorithms *(4)* know you better than you know yourself. I can’t fix the past, all the ways we have been hacked along our history, **but we can change the future, we can realize that technology is just a mirror to our society and stop blaming it, it doesn’t need fixing, we do.**

I am trying to understand all our drives *(5)*, looking from paleoarchaeology to current neuropsychology. From historians, journalists, philosophers, thinkers… to try and build, as a designer, into action and not theory, a way to create awareness on this topic. We have thrived as a collective, not as the fierce competition some believe in *(6)*, and as such, this topic also needs the combination between individual and collective action.  

*More questions arose: who am I? What are my masks? How many aspects do I have? Is my online self similar to my self? Is my self also an imaginary construction? How can I really know myself past my stories? Are believes systems influencing our personality? How much? Can I get rid of them to see my real self? Is the self also a construction?*

### Intervention:

![]({{site.baseurl}}/helloiam.jpg)


I believe so much in the social importance of the topic that I am addressing in my project. I have always thought that to be able to generate a crucial change in humanity’s path, what needs to change is humans.
I see more and more the weak signals about it, new generations that  come with a new discourse about awareness, love, society. I see the trends about self worth and self love, a growing number of techniques to self improvement, to get to know yourself.
I, as a designer for emergent futures, think that the path to follow now is to discover how to make a design intervention to push forward those “positive” weak signals. Some time ago I was relentless in doing a 1st person experience, because I see the urgent need for a collective intervention. The reflection that I made in the past weeks has lead me to think that the first step in this project is my own self-intervention. Apply what you preach and all. Also I have in myself the perfect subject to intervene, as I have said before, first individual and then collective is the way I believe this issue should be tackled. So my next step is to put myself in the center, explore all the versions on myself (online, offline), and see if I manage to unhack me, before trying to unhack you. **Can I stop escaping reality?**

### State of the art:

I feel a duty to really explain and take into account all the research that has nourished me and take me to the point I am now, but it will make this a very long article since my project takes on such numerous fields, so, for the state of the art I have to separate them into the technical solutions, the artistic interventions and the manifestos that are talking about this issue. What I want to achieve as a designer is mixing all of those for a really disruptive intervention since I haven’t find out any project taking all of them into account.

For the manifestos I would relate to the work of Yuval Harari, Tristan Harris or Jaron Lanier among others philosophers, historians, futurists, persuasion experts, psychologists, archeologists, neuroscientists and technology researchers that have helped me identify, understand and frame this research and the issue I want to face. *(11)*

In the technical solutions I have found GOBO, a plugin developed by the MIT Media Lab’s Center for Civic Media that allows to bypass the algorithms on social media and control what you see on your feed manually. Other like Feedless aim to block content from the social media feed. New social media like duckling, litsy or dribble; or create your own with mighty networks.

For the artistic interventions I have found the work of Sergio Albiac, a spanish artist that creates portraits using data, from social media, gps or how you describe yourself. I am not going deep into the 21sr century self portrait artistic research explanation as I am not doing an intervention on that. I am a designer using the artistic resource to communicate the idea, it is just an asset to create my fiction and transmit the experience, nor I believe is really useful for the reader of this article. I do want to tell that I have founded the self-portrait as a continuous asset of the artist and it has evolved to intake new technologies such as the use of data as Albiac does to the use of 3D printers and others. The work of Albiac as he communicated in the interview I had with him, searches to make the people ask a question, not to resolve it, that I believe is the integral difference between designers and artists, but our work can be fundamental in the same place and aiming to move people from the same angle.

![]({{site.baseurl}}/sergioalbiac.jpg)
*( Sergio Albiac portrait generated by GPS data )*


One important project that I want to mention since it fits in all categories is Jennifer Lyn Morone INC,  She has made herself a corporation, seeking to make profit with her own data and not giving it away for free so others take advantage.

<iframe title="vimeo-player" src="https://player.vimeo.com/video/98300179" width="640" height="360" frameborder="0" allowfullscreen></iframe>

“Life Means Business Jennifer Lyn Morone, Inc has advanced into the inevitable next stage of Capitalism by becoming an incorporated person. This model allows you to turn your health, genetics, personality, capabilities, experience, potential, virtues and vices into profit. In this system You are the founder, CEO, shareholder and product using your own resources” and her project DOME: “Because there is a monetary value to your data, that is why many corporations are able to offer their services for free. By mining, collecting and indexing as much data about oneself as possible you can gain valuable insights and intelligence specific to your operation and monitor for security threats at the same time. Having a holistic overview of your operation allows you to correlate relationships between seemingly unrelated events and activity, increasing your value.”

I believe in the importance of this exercise also, so we can see the scope how much importance the data we generate has, and why others profit from it just by offering us a service that is shaped just to keep us feeding the wheel.

And off course the [Replika AI project](https://replika.ai/), an Ai chatbot that will learn from you to be you and creates this dialogue between you and yourself to help improve cases of depression, anxiety, social issues and else. I feel this is also a very good thing in making awareness of who you are, and you helping yourself to be better. At least in this practice there is no need of a stage and special time, it can be used as often as social media is used. I like that version of a self improve practice in the everydayness available in the technology that exacerbates the same problem is trying to tackle. So this is the project closest to mine in this way.

### Future:

Escaping reality or knowing yourself. As I go deeply through this research on surveillance capitalism *(7)*, attention protection, manipulation and all the weak signals I could identify, I have found out a very thin line in the human behavior towards this 2 processes. **Either you escape reality, or you know yourself**, and sometimes to know yourself is also an escape and vice versa, as we can see in most religious and spiritual practices.

Identity and the self-image we construct can also be a distraction in the process of knowing who we really are, facing it consciously, can help us in a cathartic experience to identify those aspects that we show but are not really ours, and the ones that are ours that we can be made aware to not let manipulation come into play on them . Deconstruction of the stories we believe, outside as much as inside of us is a thing I believe necessary to make a change in these behaviors.  

![]({{site.baseurl}}/infography.jpg)


For the future I would like to envision the shift that will bring a change on how humans behave towards themselves, as utopian as it may sound, I also believe this is the time to dream high, between all the negative narratives and discourses we are facing now, as needed as they are, I would like to envision a positive future, where humans have improved and as I believe technology has improved with us as we change how we use it and how we need it. That is my vision and drive. But to take things more “down to earth” I would like to explain the practices that I am taking into account to inspire/inform my intervention and shift in the near future.

For this exercise I am going to explore 2 current practices and how I envision them evolving in the future. First is the practice of escaping reality, how we as humans have the need to shut down and recharge, but I believe that practice doesn't need to change, what needs to change is the reason why we do it, the recharge benefit of unplugging versus the unfulfilled emotion that drives us to keep escaping and not dealing with ourselves. So I believe the need and drive is what need change and not the practice itself. When we manage to change that, a lot of practices can stay the same and not have the negative impact they are currently having. For that purpose I am exploring another kind of practice, how we know ourselves and how can I envision helping to know you better with the resources of the 21st century and the digital age, and how those same resources are being used to manipulate us, how we can shift that without changing them to make them be at our service and not the other way around.

We escape because the system is designed to capture our attention and keep us using whatever is the way you decide to escape. We escape not to rest or recharge, or not only, we escape to get to oblivion, we escape to escape from ourselves, from our own unfulfillment. We use drugs, alcohol, parties, social media, streaming services, music festivals, relationships.
I want to focus on the social media use. We create this "online self", this image of ourselves showing carefully what we want to show, sometimes to get the most attention possible, sometimes to compare ourselves to others, there are a lot of hidden drives for this persona we create, most of them unconscious. One thing is true, we are not aware of the scope that this habit can have in our lives, and for the new generations this is creating more and more complex issues.

Today there is a lot of ways in which people try to know themselves. Meditation, spiritual silent retreats, spiritual drugs, horoscopes, mindfulness, self help books and seminars and so on.

![]({{site.baseurl}}/mindfullnessngram.PNG)
![]({{site.baseurl}}/selflovengram.PNG)
![]({{site.baseurl}}/knowyourselfngram.PNG)

It has the tendency to create a superiority in the people practicing it versus the ones that don't. It needs an artificial space. A setting up, rituals and a lot of outside things, things that are more distraction than other things, and for most people just another excuse of a story to create more ego masks, like the online self.

Getting to know this online persona could help us know ourselves, know all those drives that makes us show ourselves in some way or another, seeing through the mask we put in front may help us reach beyond. Doing that could help us be more aware of ourselves, and less manipulative, maybe we would rather be with ourselves than be online. Even if, as I said, the technology is not an issue. In this derided scenario, we would be online for other purposes, connection with others, we will share ourselves in a more honest way.

So my transition path to my desired futures is not a straight line, because in human behavior evolution it's never like that, it’s more of a spiral going up and down forwards and backwards taking several attempts and for each one of us a different kind of roller coaster. That is also why I believe more in the individual part of my intervention, and creating a technique that each individual can use in their own pace, and not a communal recipe as other old practices have attempted, as one of my main drives is for humans to realize that each one has their own time, at their own pace, their own way, their own purpose, maybe that can also help the unfulfillment in our lives, stop the comparison and complaining, and stop the escaping. When I talk about the collective action is to spread, transfer, exchange and help others go through the experience but it’s totally an individual action.

![]({{site.baseurl}}/FAF.jpg)

[Algorithms](https://vimeo.com/249633335) as any tech have the capacity to be for destruction or for construction, in equal.  Connectedness can help boost our own ability to improve collectively,  together we thrive *(6)*, so social media has the capacity to help us, as it has already, accomplish marvelous things in the community level, that only if we can use it individually as a tool at our service and not the other way around.  Technology is a tool that can help us solve the problems we got ourselves into. How can we shift it to benefit humanity. That is on us, the forger of the futures.

What if an algorithm could help you with the data it gathers from you??
What if the algorithm could predict if a person is homosexual and starts to send them stuff about emotional support and pair them with influencers about overcoming social  mandates and coming out. What if a teenage girl that is at risk of suffering from anorexia can be prevented by pairing her with positive body narratives instead of supermodel adds.
What if they track our health and before it happens they can send us to a doctor or prevention medicine.
That’s the difference between listening as  big brother, or really hearing me.
I believe we can get there.

### Design action and results :

<iframe width="560" height="315" src="https://www.youtube.com/embed/VGIP09L6fAE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

My design action is going to be divided into three parts. ‘The portrait’ which would be an interactive experience through a mirror in which you would see yourself through a filter generated by my data (since the data is not easy  to get quickly I cant use your own) that would transform your image so you can see the analogy of the online self reflected on the physical self, and the dialogue the two can take to enable you to be aware on how much that online self is the self. Then would be the tale of my own experience dealing with the data, getting that dialogue on, seeing myself through time, sharing my personal experience in the mirror. Finally [‘the recipe’](https://mdef.gitlab.io/katherine.vegas/reflections/recipe/) if you want to replicate it.

### Final reflection:

In the past weeks analyzing my data I came to discover that is a very hard process. getting the data is the easiest part, the going thought it, that is another thing, they usually deliver you the data in .jason format, a code format with no format at all, or an html file in which you can browse through it all separated from the other. It’s not an easy process, I would like to facilitate the transition between how we can use our data today and how it can be made easier in the futures.

Going through the data I founded  my locations, all my uploaded pictures, states, but what had me the more interested was two of them, my add tag list, that means all the categories they have me listed as interested in, and my messages and comments, because on how I write, is a more profound expression of myself and in a more “private” way vs how I show myself in public or semi public images.

![]({{site.baseurl}}/adds.JPG)
![]({{site.baseurl}}/messages.JPG)
![]({{site.baseurl}}/wordcloud.jpg)

Seeing that part of myself has made me more aware of my whys and hows. Why do I get this or that add, why do I click on it, how do I talk to my friends, how does that affect everything else, but there is still a long way to go about understanding all that data and specially what do they do with it so I can be more aware of the hacking they can do with my internal drives. *(8)* **More conscious I am about myself, less they can play with me, even if they know all about my online self. I have experience it already.**

Still, now, having gone through that, I am convinced that is a very good way to improve us, our society, the issues we are facing and also the potential good it can do if we manage to change its purpose.

### Conclusion:

Would you share your inner more private thoughts with a door to door salesman? That he then will use to come to your house with a specific product for you. It can be a good thing, I don’t need to see make up commercials if I don’t use it, but are they making people who do use it buy it with no need? As I have said countless times, all technology can be use for our profit or we can be the profit of technology. I don’t believe, as a lot of my research inspirers do *(9)*, that the solution is just to stop using the technology associated with the behavior (individual or social) that I don’t like, or I think is wrong  that’s the issue that keeps on repeating itself, we need to change the cause, not the consequence, **If humanity does not improve itself, all around it will be the same.**  We live in times of great uncertainty, of system’s change, of an upcoming technological and biological revolutions along with the ecological and political crisis.
Uncertainty can make society and people go back to the things that they know, to safe spaces, even if they are no longer viable.

These situations are crying out loud for new solutions, so going back to a luddite position, for me, is not at all the solution of our problems, but starting to work on us, on the inside, on our own improvement, in all aspects of our lives, relationships with others, with our environment with our society can only improve by our relationship with ourselves.

 *You must change your life!' means, you must pay attention to your inner vertical axis and judge how the pull from its upper pole affects you! It is not walking upright that makes humans human; it is rather the incipient awareness of the inner gradient that causes humans to do so.(10)*

 ![]({{site.baseurl}}/qr.jpg)


#### References:

- 1: Yuval Noah Harari. 21 lessons for the 21st century
- 2: Jaron Lanier. How social media ruins your life, The future of our digital lives. Tristan Harris How your brain is getting hacked.
- 3: Danah boyd. Agntology and epistemological fragmentation.
- 4: Panoptik foundation. The monologue of the algorithm.
- 5: Peter Nowak. Sex,bombs and burgers.
- 6: Douglas Rushkoff. Evolution is not the cause of selfish capitalism.
- 7: Shoshana Zuboff. The age of surveillance capitalism.
- 8:  Wolfie Christl and Sarah Spiekermann. Networks of Control A Report on Corporate Surveillance, Digital Tracking, Big Data & Privacy.
- 9: Paul Miller. A year offline.
- 10:  Peter Sloterdijk. You must change your life





#### 11-Other research references:

- PBS. The human face of big data.
- BBC: The century of the self.
- Patrik Wincent: What are you missing while being a digital Zombie.
- James Bridle: The nightmare of children’s videos. Youtube and what is wrong with the internet today.
- Dr Kimberly Young: What you need to know about internet addiction.
- Julian Bagini. Is there a real you.
- Rachel Metz. How to manipulate Facebook and Twitter instead of letting them manipulate you
- Search lab. Research reports.
- James Bridle. New dark age, technology and the end of the future.
- Joana Moll. The data brokers.


#### Special Thanks to:

- My MDEF family.
- Sergio Albiac for taking the time to talk to me.
- Tomas Diez for always pushing harder.
- Mariana Quintero for taking me out of my comfort zone and a lot of references
- Oscar Tomico for always respect and value our own ideas.
- Andres Colmenares for a good guidance just on point.
- Guillem Campodron for coding with me.
- Johnatan Minchin, for  always being  positive and unblock me.
- EGT, for the constant love.
- Portus Family, for believing.
- To me, to keep going.
- To the internet.
- To humanity.
