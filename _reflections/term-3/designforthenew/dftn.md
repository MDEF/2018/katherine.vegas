---
title: Design for the new.
period: April- June 2019
date: 2018-12-16 12:00:00
category: term3
published: true
---

### Practices:

For this exercise I am going to explore 2 current practices and how I envision them evolving in the future. First is the practice of escaping reality, how we as humans have the need to shut down and recharge, but I believe that practice doesn't need to change, what needs to change is the reason why we do it, the recharge benefit of unplugging versus the unfulfilled emotion that drives us to keep escaping and not dealing with ourselves. So I believe the need and drive is what need change and not the practice itself. When we manage to change that, a lot of practices can stay the same and not have the negative impact they are currently having. For that purpose I am exploring another kind of practice, how we know ourselves and how can I envision helping to know you better with the resources of the 21st century and the digital age, and how those same resources are being use to manipulate us, how we can shift that without changing them to make them be at our service and not the other way around.


#### Escaping reality.

- **Current Practice analysis:**

We escape because the system is designed to capture our attention and keep us using whatever is the way you decide to escape. We escape not to rest or recharge, or not only, we escape to get to oblivion, we escape to escape from ourselves, from our own unfulfillness. We use drugs, alcohol, parties, social media, streaming services, music festivals, relationships.
I want to focus on the social media use. We create this "online self", this image of ourselves showing carefully what we want to show, sometimes to get the most attention possible, sometimes to compare ourselves to others, there are a lot of hidden drives for this persona we create, most of them unconscious. One thing is true, we are not aware of the scope that this habit can have in our lives, and for the new generations this is creating more and more complex issues.

- Images: drunk or high people at parties, glued to the phone in the transport, binge watching netflix, playing video games with a mess around.
Connection to skills: we are our own worst enemy.
- Skills: being human.
Connection to stuff:
- Stuff: electronic gadgets, drugs, alcohol, our own hole inside.
Connection to images: we fill ourselves with outside things.

- **Desired practice analysis:**

I would love to envision the perfect world, but I will try to get a bit down to earth on the way of how much will change in our lives if this practice will change. What if we will use all these technologies still, just for very different reasons, as mentioned before.

- Images: people enjoying a party without drugs or alcohol abuse, being with yourself in the "free times", not having to fill them with app use, but still do it cause you enjoy it, not needing it, not filling yourself with it.
Connection to skills: we are our own best friend.
- Skills: being a better human, for you.
Connection to stuff:
- Stuff: same electronic gadgets, drugs, alcohol but no more hole inside.
Connection to images: we enjoy things, but we don't need them.

#### Knowing yourself.

**Current Practice analysis:**

Today there is a lot of ways in which people try to know themselves. Meditation, spiritual silent retreats, spiritual drugs, horoscopes, mindfulness, self help books and seminars and so on.
It has the tendency to create a superiority in the people practicing it versus the ones that don't. It need an artificial space.

- Images: Meditating sitting in the lotus position in a natural environment.
Connection to skills: needs skills to create images
- Skills: learning how to meditate, sitting in a specific position.  
Connection to stuff: needs stuff to fulfill skills
- Stuff: meditation classes, guided meditation audios, cushion, candle.
Connection to images: ritual imaginary.


**Desired practice analysis:**

Getting to know this online persona could help us know ourselves, know all those drives that makes us show ourselves some way or another, seeing thru the mask we put in front may help us reach beyond. Doing that could help us be more aware of our selves, and less manipulative, maybe we rather be with ourselves than be online. Even if as I said, technology is not an issue. In this derided scenario, we would be online for other purposes, connection with others, we will share ourselves in a more honest way.

- Images: analyzing your data and understanding your deepest drives.
Connection to skills: needs the skills to be real
- Skills: use of digital devices.
Connection to stuff: need the stuff to complete the skills
- Stuff: archive data, tutorials for coding and extracting the important part of the data files.
Connection to images: creates images.


### Intervention:

I believe so much in the social importance of the topic that I am addressing in my project. I have always thought that to be able to generate a crucial change in humanity’s path, what needs to change is humans. I see more and more the weak signals about it, new generations that  come with a new discourse about awareness, love, society. I see the trends about self worth and self love, a growing number of techniques to self improvement, to get to know yourself. I, as a designer for emergent futures, think that the path to follow now is to discover how to make a design intervention to push forward those “positive” weak signals.

My design action is going to be divided into three parts. ‘The portrait’ which would be an interactive experience through a mirror in which you would see yourself through a filter generated by my data (since the data is not so fast to get I cant use your own) that would transform your image so you can see the analogy of the online self reflected on the physical self, and the dialogue the two can take to enable you to be aware on how much that online self is the self. Then would be the tale of my own experience dealing with the data, getting that dialogue on, seeing myself through time, sharing my personal experience in the mirror. Finally ‘the manifesto recipe’ if you want to replicate it.

In the past weeks analyzing my data I came to discover that is a very hard process. getting the data is the easiest part, the going thought it, that is another thing, they usually deliver you the data in jason format, a code format with no format at all, or an html file in which you can browse through it
all separated from the other. Its not an easy process, I would like to facilitate the transition between how we can use our data today and how it can be made easier in the futures.

For the futures I would like to envision the shift that will bring a change on how humans behave towards themselves, as utopian as it may sound, I also believe this is the time to dream high, between all the negative narratives and discourses we are facing now, as needed as they are, I would like to envision a positive future, where humans have improved and as I believe technology has improved with us as we change how we use it and how we need it. That is my vision and drive.

So my transition path is not a straight line, because in human behavior evolution it's never like that, its more of a spiral going up and down forwards and backwards taking several attempts and for each one of us a different kind of rollercoaster. That is also why I believe more in the individual part of my intervention, and creating a technique that each individual can use in their own pace, and not a communal recipe as other old practices have attempt, as one of my main drives is for humans to realize that each one has their own time, their onw pace, their own way, their own purpose, maybe that can also help the unfulfillness in our lives, stop the comparison and complain, stop the escaping.
