---
title: Recipe
period:
date: 2018-12-16 12:00:00
category: final
published: true
---

### How to know your online self.

#### Download your Data:

In my path to discover my online self, I downloaded all my Data from all the "free services" that I use, google, facebook and instagram. Each one has it's own path to get it.

**Gooogle:**
*Taken from google account help*

You can export and download your data from the Google products you use, like your email, calendar, and photos. In a few easy steps, create an archive to keep for your records or use the data in another service.

Note: Downloading your data does not delete it from Google’s servers. Learn how to delete your account or how to delete your activity.

- Step 1: Select data to include in your archive:
Go to the Download your data page. Products that have your data are automatically selected.
If you don’t want to download data from a product, uncheck the box beside it.
If you only want to download some of your data from a product, you may have the option to select a button like List All data included. Then, you can uncheck the box next to data you don’t want to include.
Select Next step.

- Step 2: Customize your archive format:
Delivery method: Send download link via email, Add to Drive, Add to Dropbox, Add to Microsoft OneDrive, Add to Box
Export type: One-time archive, Scheduled exports, File type, Zip files, Tgz files
Archive size: Choose the maximum size archive you want to create. If the data you’re downloading is larger than this size, multiple archives will be created.

- Step 3: Get your archive:
When your archive is created by using one of these options, we'll email you a link to its location. Depending on the amount of information in your account, this process could take from a few minutes to a few days. Most people get the link to their archive the same day that they request it.

Note: If you’re enrolled in the Advanced Protection Program, your archive will be scheduled for two days in the future

**Facebook:**
*taken from Facebook help*

To download a copy of your Facebook data:

- Go to the top right of Facebook and click .
- Click Settings.
- Click Your Facebook Information.
- Go to Download Your Information and click View.
- To add or remove categories of data from your request, click the boxes on the right side of Facebook.
Select other options, including:
- The format of your download request.
- The quality of photos, videos and other media.
- A specific date range of information. If you don't select a date range, you'll request all the information for the categories you've selected.
- Click Create File to confirm the download request.
After you've made a download request, it will appear as Pending in the Available Files section of the Download Your Information Tool. It may take several days for us to finish preparing your download request

Once we've finished preparing your download request, we'll send a notification letting you know it's ready.

To download a copy of data you requested:

- Go to the Available Files section of the Download Your Information tool.
- Click Download and enter your password.
- You can also click Show more to view information about your download request, such as the format and when it will expire.

Note: You can always view your Privacy Shortcuts to learn about the ways you can control your data and privacy on Facebook. If you want to review recent activity on your Facebook account or want to review your Facebook account information, you can use the Access Your Information tool.

**Instagram**
*taken from instagram help*

You're in control of your data on Instagram. You can view your account data or download a copy of your data on Instagram at any time.

Reviewing your data on Instagram

From Instagram on the Web:
- Go to your profile and click
- Click Privacy & Security
- Scroll down to Account Data and click View Account Data
- To review a specific type of data, click View All.

From iOS or Android:
- Go to your profile and tap
TapSettings
- Tap Privacy and Security > Access Data
- To review a specific type of data, tap View All.

Downloading a copy of your data on Instagram

If you want a copy of everything you've shared on Instagram, you can request a download of your data in a machine readable (JSON) format. You'll need your Instagram account password to request this information. Learn more if you've forgotten your password or can't log in.

From Instagram on the Web:
- Go to your profile and click
- Click Privacy & Security
- Scroll down to Data Download and click Request Download
- Enter the email address where you'd like to receive a link to your data and enter your Instagram account password
You'll soon receive an email titled Your Instagram Data with a link to your data. Click Download Data and follow the instructions to finish downloading your information.

From iOS or Android:
- Go to your profile and tap
- TapSettings
- Tap Privacy and Security > Download Data
- Enter the email address where you'd like to receive a link to your data and tap Request Download
- Enter your Instagram account password
You'll soon receive an email titled Your Instagram Data with a link to your data. Click Download Data and follow the instructions to finish downloading your information.

Note: It may take up to 48 hours for us to email you a download link. Some data you have deleted may be stored temporarily for safety and security purposes, but will not appear when you access or download your data.
If you can't access your Instagram Account and still want to download a copy of your data, you can contact us.

**For other services search in your favorite engine**

#### Going through the Data:

I first downloaded all the data in a HTML format, that opens a browser in which you can see all the categories and files, in Facebook as in Google, and start to see how much information they have on you. There is your locations, messages, comments, files, photos, all your Youtube searches, web searches, games, the notes, posts, ads categories they have you on and more.
You can go easily through the browser and see all the things, but they are all detached from each other, the messages on Facebook are classified by friends and groups. So you don't have access to all the data together.
If you download the data in JSON format like Instagram does (Facebook and Google have the option too) you will get the code format of your data. You can go through it, you can [pretty print](http://jsonprettyprint.com) it to make it easier, or you can extract the data.

To do that the first thing is to [see the structure](http://jsonviewer.stack.hu/) then you need to make a code that takes only what you need from it.
I did it with the messages with the help of my classmate [Ilja](https://mdef.gitlab.io/ilja.panic/) , I wanted to see only the messages, not the dates or sender or etc.

![]({{site.baseurl}}/code1.PNG)

![]({{site.baseurl}}/mess1.PNG)

![]({{site.baseurl}}/mess2.PNG)

I did the same with the comments.

Then another big deal for me was to see all the cluster I was in for advertisements, in facebook you have 3 categories, the one that you have interacted with, for me was very small, then the add interests, and the ones that have uploaded you as a contact. That goes very crazy, you can find something that are not related to you, yet you are there some how, and thing that you say ok, yes I like that.

![]({{site.baseurl}}/adds.JPG)

You can find help to extract and get a better view of your data in this examples.
[1](https://github.com/ownaginatious/fbchat-archive-parser)
[2](https://github.com/kaustubhhiware/facebook-archive)
[3](https://towardsdatascience.com/mapping-my-facebook-data-part-1-simple-nlp-98ce41f7f27d)

#### Final Advice:

It's a long process, and they don't make it easy, but if you are convinced like I am, you can have patience and start to observe what happens in your social media in regard of what you write or talk on WhatsApp. I for instance was talking to my mom once and she suggested that I buy a bamboo toothbrush, then on Instagram I started to have a lot of adds from different kinds of bamboo toothbrushes. So sometimes it can be use full, others is not, in any case the point of this exercise is to be aware of why and how your online self behaves to see how much impact it has on your life. How much addiction to the dopamine you have relates to hoy much you check and seek for likes and views, that dopamine is also relates to if you are not making it by your own with the things that make you happy on your life.
I am not in any case a life coach or similar, I am just inviting you to take a look at this thing you are constantly creating, that is a reflection on yourself, one way or another. So put the tool at your service.

I am available for discussions on the email below.
