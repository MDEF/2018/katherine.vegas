---
title: Emergent Business
period: April-June 2019
date: 2018-12-16 12:00:00
category: term3
published: true
---

### Session 1:

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSHG5rL2Pn3YhtqdMh3gXIBWXBeItNd8RqoW7WJ0LuZ9gMcNOXVSc5IqpWZ6_0FmRzpjqIqZcr65qda/embed?start=false&loop=false&delayms=10000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>


### Session 2:

![]({{site.baseurl}}/newspaper.jpg)


### Reflection:

This course was very helpful on seeing how today and in the future new business models are emerging.
We analyzed and saw examples on how in the past a small change has had a bigger impact making a huge change in a system, like washing the hands did in hospitals or the simpler line divider in roads helped in traffic crashes. We also saw how new business models have had the same or more impact in very less time than old ones, like comparing Airbnb and Hilton hotels.

All of those impacts can be done by analyzing the expanded ecosystem of the model, the level in which you want to make the impact, how to minimize internal and external efforts and still have the maximum impact. To challenge a dominant regime with a value producing network.
We have now no trust in the traditional systems, at the same time we have developed more informal cooperation, open source projects, and the value of the traditional assets in a business has change to adapt to new demands.
I like this course because, as much as we don't want it, we live still in a capitalist world, and for some of our projects to thrive now and in the near future, we need this kind of vision and knowledge.
The pentagrowth model developed by Ideas for change explained in class I think is a good way to understand and apply those previous concepts to develop something that is at the same time good for the people, disruptive and profitable. Is a growth based on value and not on capital, that for one is a small but good change of drive in my point of view. It helps too to see into the future on where do you (project) want to be and how to get there in a realistic and valuable way.
