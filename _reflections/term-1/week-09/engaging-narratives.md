---
title: 09. Engaging Narratives
period: 26 November - 2 December 2018
date: 2018-12-02 12:00:00
category: term1
published: true
---


This was a very intense and confusing week for me. It was awesome having the possibility to connect with Heather Corcoran from kickstarter and learn how to promote properly a project to get people’s attention. How to tell a story that is authentic and gather an audience. For me sometimes in really hard to know how to promote something, how to get people interested, I have been trained in my professional life to sell a design to a client, but that’s a bit different I think, even with the common points.

After that with Bjarke Calvin we got to know duckling, an amazing “insight media” as he likes to call it and I find really appropriated. we did the exercise of telling a story based on all the knowledge about documentaries he told us. I chose this one:

![]({{site.baseurl}}/en11.jpg)
![]({{site.baseurl}}/en12.jpg)
![]({{site.baseurl}}/en13.jpg)
![]({{site.baseurl}}/en14.jpg)


 I really liked his vision on the issues of social media, who we are in the view of our followers, the shallowness, sometimes necessary, and how many parts we as human have, as the ugly duckling story, we are a complex world to be discover, and I loved the analogy.  

### Reflection:

This week most important exercise was to tell the story of our area of interest or if we have it the area of intervention. Especially for me was a hard confrontation since I yet not had one. I did this story about discipline I a m interested and why I think it’s important to address this way of facing problems and get out of the data and scientific approaches, trying to show it in a different way of that of a power point presentation. I did it from the fairy tale style, but sci-fi, trying to tale a story with a lesson. And as I talked with Mariana Quintero, trying to change the dystopian discourse about the future that somehow has taken over and do a more positive narrative of the futures.


![]({{site.baseurl}}/en21.jpg)
![]({{site.baseurl}}/en22.jpg)
![]({{site.baseurl}}/en23.jpg)
![]({{site.baseurl}}/en24.jpg)
![]({{site.baseurl}}/en25.jpg)
![]({{site.baseurl}}/en26.jpg)



Then after talking with Tomas Diez, I had to define an area of interest, after much debating and research to try to figure out something that can share my various interests and keep it on the field of Human behavior and the development of habits, I came up trough an “epiphany” with the topic about **escaping reality.** The entertainment industry such as Netflix, tv, movies, social media, reality tv, games and other parts of it like parties, drugs and even religion. The addiction we face on such issues and levels of them, how can it afflict personal relations, consequences of such.

#### Questions arose:

Why the need to scape? Why the world doesn’t fulfill us? What habits have change to makes us have free time to spend on that and how can they change in the futures? What society standards affect that reality? Where does addiction come from? How do we cope with addictions, define and pass levels of it? What is the relation with unconscious desires that can be hacked? What is the relation with the free will? How will VR or augmented reality affect it? Does procrastination have a say in this? And much more.

**We will see what comes out of it. But I am exited!!**
