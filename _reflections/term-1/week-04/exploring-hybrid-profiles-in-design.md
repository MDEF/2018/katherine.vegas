---
title: 04. Exploring Hybrid Profiles in Design
period: 22-28 October 2018
date: 2018-10-28 12:00:00
category: term1
published: true
---


### Domestic Data streamers:

Creating meaningful connections between information and people.
I learned about how a business is made from scratch, how they started and landed in this field about data. The projects they have done. The link they need between different professions and approached to develop projects and to maintain the business. Redefining what they do constantly. From shaping experiences that can impact and change behaviors to interactive publicity.
In the exercise we did with taking what was in our pockets I realized how much of our own history is in the things we carry, and how that information can be applied from different approaches. How to manage Data.

![]({{site.baseurl}}/hp1.jpg)
![]({{site.baseurl}}/hp2.jpg)
![]({{site.baseurl}}/hp5.jpg)




### Ariel Guersenzvaig:

The ethics in designing.
I learned about all his professional path, how it has change in more than 20 years, from informatic science to user experience, design and communication and design coaching to the ethics of designing. He also talked about how the internet has evolved, and web designing with it. New professions developed from there. Design decision making, how experts to novices’ creative processes are. New methods of resolving problems, wicked problems, reframe problems. No solution is right or wrong but better or worse. Relying on intuition, but an intuition that is grounded and tested. The dreyfus expertise. And from that how designers should act: professional design ethics.  The intuitions that you don’t have yet about a topic. Design defines how people should live, so we have a pretty big responsibility. The capabilities approach justifies design to increase people’s capabilities and focus on this and not reasons.
Then we had a debate about “killer robots”, how to look at a problem from the other side (the one we don’t agree on) to have better arguments and solutions from the one we have.

![]({{site.baseurl}}/hp3.jpg)

### Vision and identity. Tools and techniques for self-reflection:

-	I liked the development of experiences, how to impact people, to make awareness on a topic not well known governmentally
-	I love the study of human behavior to develop experiences.
-	I like to think on the impact of design, and the not foresee used of it.
-	I like the study of the creative process in design, and the design decision making.
-	I like the ethics of design, what way and how design should be. Responsibilities.
-	I like the installations and interventions in the physical world and learn how to make them.


### Personal Development plan:


#### How the courses can help me improve myself:

1: Articulate and present me better.
2: Do things by scientific method. Explore biophilic design
3: Making, using machines, repurposing objects
4: seeing other’s paths, other’s ways, field. Exploring me. Develop my direction and vision.
5: Clarifying fields of interest by learning to see through the uncertainty
6: How can AI help us and our experiences.
7: Learning how things work and how they can be improved, redesigning.
8: Develop a project by first person experience.
9: Articulate and present better my ideas and projects.
10: Clarifying where to develop a project.
11: Making, prototyping.
12: Present and talk.
My goal is to learn to develop substantial experiences. Anything that helps me do that will be welcome, knowing more what is happening in this field around the world.

### How can I create direction for myself and my projects?

#### Vision:
by learning from our mistakes, gathering data, jump through phases, speculating, integrating approaches, giving space to recover, open design and flexibility.

#### Reflection:
 by annotation, examples, guidelines, constructive critic, tacit to explicit tools and method.

#### Experience:
 by not imposing our personal views, having openness and flexibility, being in the real, not falling to ethnographic trap and balancing capabilities and resources and changing from third to second to first person approach.

![]({{site.baseurl}}/hp4.jpg)



### VISION:

#### The person is the agent of change, the tool is the identity:

My vision is to be a designer which through analysing human behaviour can improve existing experiences, create and develop new ones and can make change that start with individual action. I also want to share my knowledge of this approach and teach new designers, forming a network in order to develop the field further. Some research areas I would like to explore include ethics, the creative process and experience design which all work towards my vision of making design accessible that I call ‘designocracy’.
