---
title: 03. Design for the Real Digital World
period: 15-21 October 2018
date: 2018-10-21 12:00:00
category: term1
published: true
---

In this week we were task to crate and personalized our class room space while learning the basic use of the fablab.

### PROCESS


We started separated in groups according to skills to have balance in every group.

Working in this first plan of desining and conceptualizing a project for me was very easy since it is what I do and have worked on for the past 6 years. We decided to begin by doing a list of needs and wishes for the space, then of those taking guidelines that let us to find the concept of “MDEF plaza”.
Having a clear concept is easy to sketch ideas because you know how the space should have, work and look, how you want to use materials and you should have clear guidelines that make the process go on a clear path.  For our group, one of those guidelines, was using as much recycling material as possible, designing from the material.


![]({{site.baseurl}}/dr1.jpg)
![]({{site.baseurl}}/dr2.jpg)
![]({{site.baseurl}}/dr3.jpg)
![]({{site.baseurl}}/dr7.jpg)


In the groups presentation I noticed that all of us wanted pretty much the same things. A different way to attend lessons, a place to make, a place to relax, a place to store, a place to work. then we voted on our preferred designs from all the groups, it was very consensus too.
The task for our group was a movable wall, a storage space, bean bags and the recycling point.
And then the making started. For me it was something new, I have designed furniture before, I worked on a very large furniture company also. But I have never worked with my own hands on it. So, it was great, I was really looking forward to doing it, to learn the use of the tools, but specially to use the CNC, the laser cut, the 3d printer.

![]({{site.baseurl}}/dr8.jpg)
![]({{site.baseurl}}/dr9.jpg)


### RESULTS               

The group work for us was very focused, and an easy-going process since we had clear goals, we knew everyone’s skills and could rely on them, we shared tasks and knowledge and divided forces to conquer the most. Still the time was short, so we focused on the wall and storage, the bean bags and the recycling point are to de finished in the upcoming weeks.

![]({{site.baseurl}}/dr4.jpg)
![]({{site.baseurl}}/dr5.jpg)
![]({{site.baseurl}}/dr6.jpg)


### CONCLUTIONS

At the end presentation the space was amazing! Every design from each group was very well done, the coffee point with the Arduino technology for the mushrooms, the multipurpose tables, the maker space, the wall, the storage, the balcony and the new display of the tables really come together to reflect what we as a group need and want of the space. I am really proud of what we accomplished this week, team work, collaboration between backgrounds, learning and doing with your own hands, everyone did something, found a place to come together and improve what is going to be our space these months. I really appreciated the opportunity to make it, to personalize it according to us, the users, and to realize how much you can do with “scraps” from the streets, how something can be repurposed to be something completely different.

#### Form follows function but re-function follows material action and subject.
