---
title: 12. Design Dialogues
period: 17-23 December 2018
date: 2018-12-23 12:00:00
category: term1
published: true
---


### My area of interest:

![]({{site.baseurl}}/dd1.jpg)

To explain it I choose to display all my research in a video collage that tells the story of all the stages I passed in it. From attention, addiction, social media, Netflix to the manipulation, the true self and the trends that we see in new technologies like augmented and virtual reality, to the description of futures scenarios that gave us the sense that the path being followed is not the best, and maybe glimpse at how I can do an intervention about it.

 <iframe width="560" height="315" src="https://www.youtube.com/embed/9hn5RMs-dqg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

This is how I choose to relate the courses that we have seen on the 1st term to my research and how they have helped me with the tools of discerning, understanding, invasion and exploring the futures.

 <iframe width="560" height="315" src="https://www.youtube.com/embed/SBMuA5QoBqs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Feedback:

Social support for addictions. The reinforcement about the individual in the past times. How to use mass consumption to generate change.  Explore different outputs from the same input. Using VR to generate empathy. Mel Slater. What to do better with our time. The relation with our brain, how does it work? Use of VR too explore our consciousness. How VR relate to work. Research about the use of LCD in silicon valley in the 60’s. Psychologic utopias. How to hack the mind trough yourself. Modify behavior with VR. Self edition.  Author of yourself. We can get addicted to anything. Emotional education. Tolerance, desires, experiences. What do you win/lose when you escape. What is real.

### Conclusions:

After the talk with the guests and a lot of amazing feedback, I have to say that I am being inclined to do an intervention about ourselves. In that I mean, the discovery of our potential, the freewill, the sovereignty of our conditions, the antimanipulation and hacking by corporations, and how can we use all those technologies for that purpose. I know VR is coming in a big big way. But more than just work with the technology I would like to work with the human, with the help of tools such as technology, and I feel that now, every aspect of my life, professional and personal is coming together in this project and I feel it more special and with great potential. **Now I know my purpose here. I always knew everything will come together.**
