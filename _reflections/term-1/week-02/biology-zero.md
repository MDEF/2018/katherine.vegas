---
title: 02. Biology Zero
period: 8-14 October 2018
date: 2018-10-14 12:00:00
category: term1
published: true
---


### How to connect experience with biology?

I began the course and my research not knowing how I will apply all those things about the fields of science, molecules, macromolecules, microorganisms and DNA that we learned this week in my field of interest. Then I stumbled upon some interesting articles on biomimicry, and on that field the process biomimicry, and biophilic design. Biophilia defined as human evolutionary affinity for other living organisms and processes by EO Wilson.  He also hoped that an understanding and acceptance of our inherent love of nature, if it exists, might generate a new conservation ethic.

I found 3 articles that led me to some interesting questions:



> “Design and perception: Making the zoo experience real” in which “The purpose of this paper is to encourage the application of theories of human behavior to zoo design so that zoo visitors are environmentally predisposed to learn from and enjoy what they experience. The ultimate goal is to increase public awareness and appreciation of the importance of habitat and its protection to wildlife conservation and to present zoo animals in such a way that their reason for being and rights to existence are intuitively self‐evident to viewers. Many of the concepts and guidelines presented appear to be suitable subjects of behavioral research, whose findings would assist designers and other zoo professionals in continued improvement of the zoo visitor's experience.”
Coe, Jon. (1985). Design and Perception: Making the Zoo Experience Real. Zoo Biology. 4. 197 - 208. 10.1002/zoo.1430040211.



>  “Design lessons from evolutionary biology” in which “UX design benefits greatly from research surrounding market segmentation, personas, and analytics designed to understand the nuances of particular users. But what if design drew on insights from human biology that transcend segment preferences? Can delightful user experiences draw upon our innate, evolutionary tendencies rather than just current preferences and trends that are unique to particular groups of people? Indeed, evolutionary tendencies have been found to underpin consumer behaviors and such considerations may be the key to timeless and universal considerations in UX design.”
>  [http://www.uxbooth.com/articles/design-lessons-from-evolutionary-biology/](http://www.uxbooth.com/articles/design-lessons-from-evolutionary-biology/)



>“Collective unconscious: How gut microbes shapes human behavior. In wich “The human gut harbors a dynamic and complex microbial ecosystem, consisting of approximately 1 kg of bacteria in the average adult, approximately the weight of the human brain. The evolutionary formation of a complex gut microbiota in mammals has played an important role in enabling brain development and perhaps sophisticated social interaction. Genes within the human gut microbiota, termed the microbiome, significantly outnumber human genes in the body, and are capable of producing a myriad of neuroactive compounds. Gut microbes are part of the unconscious system regulating behavior. Recent investigations indicate that these microbes majorly impact on cognitive function and fundamental behavior patterns, such as social interaction and stress management. In the absence of microbes, underlying neurochemistry is profoundly altered. Studies of gut microbes may play an important role in advancing understanding of disorders of cognitive functioning and social interaction, such as autism.”
>[https://www.sciencedirect.com/science/article/pii/S0022395615000655](https://www.sciencedirect.com/science/article/pii/S0022395615000655)

### Experiment.

#### Hypothesis:

I considered, what if somehow between gut microbiome and evolutionary tendencies can help us improve some experiences. Recycling for instance. Can it be possible that humans can be more related to recycle if they don’t see it as “garbage”. Because we have a cognitive stop related to all thing that can make us sick, and waste is a big feeding soup for microorganisms that can makes us sick, so in evolution we have been programmed to stay away from it. However paper or plastic waste is not like that, but we still have some apprehension about it.

**So, cognition related to recycling methods will be improved by attributing indicators coming from human evolutionary affinities?**

#### Method:

* Neuropsychology department will identify those evolutionary cognition impediments towards trash and identify which others can help us overcome the first ones, see if maybe some people are more prone than others and why.
* Microbiology and neurology department will see how gut microbiome can affect our behavior and in which way.
* Genetic department will identify which genes are related to those microbiomes and evolutionary tendencies. They will then see if some people or communities are more prompted to have those genes therefore in need of more behavioral changes.

With those results I will develop a recycling experience that is friendlier towards ours evolutionary cognitions.

![]({{site.baseurl}}/gt2.jpg)

![]({{site.baseurl}}/bz1.jpg)
![]({{site.baseurl}}/bz2.jpg)
![]({{site.baseurl}}/bz3.jpg)
![]({{site.baseurl}}/bz4.jpg)
![]({{site.baseurl}}/bz5.jpg)
