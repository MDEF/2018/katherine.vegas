---
layout: page
title: About
permalink: /about/
---

<span style="color:magenta">HI!</span>

I was a Colombian interior and landscape designer who trained in Buenos Aires with 6 years of commercial, corporate and institutional experience. I believe that functionality and beauty is not enough in design and want to do something more valuable and with a real impact.

I have arrived in this master’s program seeking something new, to change and improve myself and the world around me.  I believe there are a lot of things that could benefit the world but are not realized due to the experience associated with them and everyday practices that are not the problem but reflect on how disruptions on our time are impacting our future based on our own lack of self reflection.

I believe the world doesn't need change, we do.

My vision is to be a designer which through analyzing human behavior and weak signals can improve existing experiences, create and develop new ones and can make change that start with individual action. I also want to share my knowledge of this approach and teach new designers, forming a network in order to develop the field further. Some research areas I would like to explore include everyday practices, ethics, the creative process and off course, the future.

I see myself working with experience, intervention, sustainability, speculation and innovation. I am a positive person about the future, I can’t wait to see it all!

<span style="color:magenta">Change is coming!!</span>
